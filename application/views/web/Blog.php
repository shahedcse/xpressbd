<article> 
    <!-- Breadcrumb -->
    <section class="theme-breadcrumb pad-50">                
        <div class="theme-container container ">  
            <div class="row">
                <div class="col-sm-8 pull-left">
                    <div class="title-wrap">
                        <h2 class="section-title no-margin">Blog</h2>
                        <p class="fs-16 no-margin"></p>
                    </div>
                </div>
                <div class="col-sm-4">                        
                    <ol class="breadcrumb-menubar list-inline">
                        <li><a href="<?= base_url(); ?>" class="gray-clr">Home</a></li>
                        <li class="active">Blog Posts</li>
                    </ol>
                </div>  
            </div>
        </div>
    </section>
    <section class="pt-50 pb-120 error-wrap">                    
        <div class="theme-container container text-center">               
            <h2 class="section-title no-margin">Coming Soon</h2>
            <p class="fs-20">Our Blog Site is coming Soon!</p>
            <br />
            <h3 class="no-margin pt-10"> <a href="<?= base_url(); ?>" class="title-1"> <i class="arrow_left fs-20"></i> go back to home </a> </h3>
        </div>
    </section>


</article>

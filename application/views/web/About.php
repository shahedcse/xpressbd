<article class="about-page"> 
    <!-- Breadcrumb -->
    <section class="theme-breadcrumb pad-50">                
        <div class="theme-container container ">  
            <div class="row">
                <div class="col-sm-8 pull-left">
                    <div class="title-wrap">
                        <h2 class="section-title no-margin">About us</h2>
                        <p class="fs-16 no-margin">Know about us more</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <ol class="breadcrumb-menubar list-inline">
                        <li><a href="<?= base_url(); ?>" class="gray-clr">Home</a></li>
                        <li class="active">About</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- /.Breadcrumb -->

    <!-- About Us -->
    <section class="pad-50 about-wrap">
        <span class="bg-text">About</span>
        <div class="theme-container container">               
            <div class="row">
                <div class="col-md-6">
                    <div class="about-us pt-10">
                        <p class="fs-16 wow fadeInUp text-justify" data-wow-offset="50" data-wow-delay=".25s">
                            Established in 2018 Parcel Xpress BD is now one of the most
                            reliable courier service in Bangladesh. We are now a delivery
                            one-stop-shop providing same day delivery and next day delivery
                            for any professional company throughout Bangladesh.
                            We also delivery parcel in Thailand.
                        </p>
                        <ul class="feature">
                            <li> 
                                <div class="feature-content wow rotateInDownRight" data-wow-offset="50" data-wow-delay=".30s"> 
                                    <h2 class="title-1">CORE VALUES:</h2> 
                                    <ul style="list-style-type: disc;font-size: 16px;padding-top: 12px;">
                                        <li>Customer Focused: Delivering on client commitment is our top priority.</li>
                                        <li>Honesty: To be transparent, open & straightforward in all our dealings.</li>
                                        <li>Excellency: We create cutting edge solutions by integrating our services with the client’s business goals. Innovation: To allow employees to be creative, innovative.</li>
                                        <li>Sustainability: Long term prosperity and continued commitment growth.</li>
                                    </ul>

                                </div>  
                            </li>
                         </ul>
                    </div>
                </div>
                <div class="col-md-6 text-center">                                
                    <img alt="" src="<?= base_url('assets/img/block/parcel2.gif'); ?>" class="effect animated fadeInRight" />
                </div>
            </div>
        </div>
    </section>
    <!-- /.About Us -->

    <!-- More About Us -->
    <section class="pad-30 more-about-wrap">
        <div class="theme-container container pb-100">               
            <div class="row">
                <div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-offset="50" data-wow-delay=".20s">
                    <div class="more-about clrbg-before" style="height: 320px">
                        <h2 class="title-1 text-center">what we do</h2>
                        <div class="pad-10"></div>
                        <p class="text-justify">Since Parcel Xpress BD began, we have taken great
                            pride in not only delivering consignments on time, 
                            but also in making a positive difference to our 
                            clients business. Customer care is in our blood here at Parcel
                            Xpress BD which is why we’re committed to always using the latest technology
                            aimed at putting the sender in control.</p>
                    </div>
                </div>

                <div class="col-md-4  col-sm-4 wow fadeInUp" data-wow-offset="50" data-wow-delay=".40s">
                    <div class="more-about clrbg-before" style="height: 320px">
                        <h2 class="title-1 text-center">our mission</h2>
                        <div class="pad-10"></div>
                        <p class="text-justify">To become the most economical courier/parcel service provider in
                            the world that offers wide range of time saving services through
                            excellence and perfection. Through our regional & international network,
                            we aim high to become one of the world’s leading courier service providers
                            through customized service that meets the changing nature of courier industry.</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
                    <div class="more-about clrbg-before" style="height: 320px">
                        <h2 class="title-1 text-center">Our Vision</h2>
                        <div class="pad-10"></div>
                        <p class="text-justify">Our vision is to provide unrivaled courier services to
                            clients around the world through professional approach
                            & delivering in time to all destinations around the world. </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.More About Us -->
</article>

